# Chat App Frontend

## Installation

### Prerequisites

- Node.js (v14.x or higher)
- npm (v6.x or higher) or yarn

### Setup

1. Clone the repository:

    ```
   https://gitlab.com/coderangel117/my_irc.git
    ```

2. Go to api directory

   ```
   cd front
   ```

3. Install dependencies:

    ```
        npm install
    ```


## Usage

### Running the Development Server

```
    npm run dev
```
Open http://localhost:5173 to view it in the browser.