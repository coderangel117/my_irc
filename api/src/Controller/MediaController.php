<?php

namespace App\Controller;

use App\Entity\Media;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;

#[Route('/media', name: 'app_media')]
class MediaController extends AbstractController
{
    private EntityManagerInterface $entityManager;
    private SluggerInterface $slugger;
    private string $mediaDirectory;

    public function __construct(EntityManagerInterface $entityManager, SluggerInterface $slugger, string $mediaDirectory)
    {
        $this->entityManager = $entityManager;
        $this->slugger = $slugger;
        $this->mediaDirectory = $mediaDirectory;

    }

    #[Route("/upload", name:"upload_media", methods: ["POST"])]
    public function upload(Request $request): Response
    {
        $file = $request->files->get('file');

        if (!$file instanceof UploadedFile) {
            return $this->json(['error' => 'No file uploaded'], Response::HTTP_BAD_REQUEST);
        }

          // Calculate MD5 hash of the file content
          $fileContent = file_get_contents($file->getPathname());
          $fileHash = md5($fileContent);
  
          // Check if the file with the same hash already exists
          $existingMedia = $this->entityManager->getRepository(Media::class)->findOneBy(['hash' => $fileHash]);
          if ($existingMedia) {
              return $this->json(['id' => $existingMedia->getId(), 'url' => $existingMedia->getUrl()], Response::HTTP_OK);
          }
  
          $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
          $safeFilename = $this->slugger->slug($originalFilename);
          $newFilename = $safeFilename.'-'.uniqid('', true).'.'.$file->guessExtension();
  
          try {
              $file->move($this->mediaDirectory, $newFilename);
          } catch (FileException ) {
              return $this->json(['error' => 'Could not upload file'], Response::HTTP_INTERNAL_SERVER_ERROR);
          }
  
          $media = new Media();
          $media->setType($file->getMimeType());
          $media->setUrl('/uploads/media/'.$newFilename);
          $media->setHash($fileHash);
        $this->entityManager->persist($media);
        $this->entityManager->flush();

        return $this->json(['id' => $media->getId(), 'usrl' => $media->getUrl()], Response::HTTP_CREATED);
    }

    #[Route("/media/{id}", name: "get_media", methods: ["GET"])]
    public function getMedia($id): Response
    {
        $media = $this->entityManager->getRepository(Media::class)->find($id);

        if (!$media) {
            return $this->json(['error' => 'Media not found'], Response::HTTP_NOT_FOUND);
        }

        return $this->json(['id' => $media->getId(), 'type' => $media->getType(), 'url' => $media->getUrl()], Response::HTTP_OK);
    }

    #[Route("/media/{id}", name: "delete_media", methods: ["DELETE"])]
    public function deleteMedia($id): Response
    {
        $media = $this->entityManager->getRepository(Media::class)->find($id);

        if (!$media) {
            return $this->json(['error' => 'Media not found'], Response::HTTP_NOT_FOUND);
        }

        $filePath = $this->getParameter('media_directory').basename($media->getUrl());
        if (file_exists($filePath)) {
            unlink($filePath);
        }

        $this->entityManager->remove($media);
        $this->entityManager->flush();

        return $this->json(['message' => 'Media deleted'], Response::HTTP_OK);
    }

    public function index(): Response
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/MediaController.php',
        ]);
    }
}
