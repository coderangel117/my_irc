module.exports = {
    development: {
        dialect: 'sqlite',
        storage: './database/dev.sqlite',
    },
    production: {
        dialect: 'sqlite',
        storage: './database/prod.sqlite',
    },
};
