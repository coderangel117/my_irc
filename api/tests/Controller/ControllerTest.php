<?php

namespace App\Tests\Controller;

use App\Entity\User;
use JsonException;
use Symfony\Component\HTTPFoundation\Response;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ControllerTest extends WebTestCase
{
    private ?KernelBrowser $client = null;
    private ?string $token = null;
    private static int $counter = 1;

    protected function setUp(): void
    {
        parent::setUp();
        $this->client = static::createClient();
        $client = static::getClient();
        $entityManager = $client->getContainer()->get('doctrine')->getManager();

        // Insérer des données dans la base de données
        $user = new User();
        $userCounter = self::$counter++;
        $user->setUsername('user_'.$userCounter);
        $user->setRole('ROLE_USER');
        $user->setPassword('$2y$10$goZjek7TlVOFFC78qab.teFmn4pIoKsz4Q3qGOI6wgViJ9lwX151a');
    }

    /**
     * @throws JsonException
     */
    public function testRegistrationSuccess(): void
    {
        // Première requête pour créer l'utilisateur
        $this->client->request('POST', '/register', [], [], [], json_encode([
            'username' => 'test_user',
            'password' => 'test_password',
        ], JSON_THROW_ON_ERROR));
        // Vérifier que la réponse a un statut 201
        $this->assertEquals(201, $this->client->getResponse()->getStatusCode());
        // Vérifier que la réponse contient les détails de l'utilisateur créé
        $responseData = json_decode($this->client->getResponse()->getContent(), true, 512, JSON_THROW_ON_ERROR);
        $this->assertArrayHasKey('id', $responseData);
        $this->assertArrayHasKey('username', $responseData);
        $this->assertArrayHasKey('role', $responseData);
        $this->assertEquals('test_user', $responseData['username']);
    }

    /**
     * @depends testRegistrationSuccess
     * @throws JsonException
     */
    public function testExistingUserRegistration(): void
    {
        $this->client->request('POST', '/register', [], [], [], json_encode([
            'username' => 'test_user',
            'password' => 'test_password',
        ], JSON_THROW_ON_ERROR));

        // Attendre une réponse 409, car l'utilisateur existe déjà
        $this->assertEquals(409, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @depends testExistingUserRegistration
     * @throws JsonException
     */
    public function testAuthentication(): string
    {
        // Envoyer une demande POST à la route d'authentification avec des données valides
        $this->client->request('POST', '/authenticate', [], [], [], json_encode([
            'username' => 'test_user',
            'password' => 'test_password',
        ], JSON_THROW_ON_ERROR));

        // Vérifier que la demande a réussi (code de statut 200)
        $this->assertSame(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());

        // Vérifier que la réponse contient un token JWT
        $responseData = json_decode($this->client->getResponse()->getContent(), true, 512, JSON_THROW_ON_ERROR);
        $this->assertArrayHasKey('token', $responseData);
        $this->assertNotNull($responseData['token']);

        // Stocker le token JWT pour une utilisation ultérieure
        return $responseData['token'];
    }

    /**
     * @depends testAuthentication
     * @throws JsonException
     */
    public function testGetUserDetails(): void
    {
        $this->token = $this->testAuthentication();
        // Vérifier que le token JWT a été généré lors du test d'authentification
        $this->assertNotNull($this->token);

        // Ajouter le token JWT aux en-têtes d'authentification
        $this->client->setServerParameter('HTTP_AUTHORIZATION', 'Bearer ' . $this->token);

        // Envoyer une demande GET à la route /me
        $this->client->request('GET', '/me');

        // Vérifier que la demande a réussi
        $this->assertSame(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());

        // Vérifier que la réponse contient les détails de l'utilisateur
        $responseData = json_decode($this->client->getResponse()->getContent(), true, 512, JSON_THROW_ON_ERROR);
        $this->assertArrayHasKey('id', $responseData);
        $this->assertArrayHasKey('username', $responseData);
        $this->assertArrayHasKey('role', $responseData);
    }

    /**
     * @depends testAuthentication
     * @throws JsonException
     */
    public function testUserWithToken(): void
    {
        //Test de la route /user sans un token
        $this->client->request('GET', '/user');

        // Vérifier que la réponse a un statut 401
        $this->assertEquals(401, $this->client->getResponse()->getStatusCode());

        $this->token = $this->testAuthentication();
        //Test de la route /user avec un token valide
        $this->client->setServerParameter('HTTP_AUTHORIZATION', 'Bearer ' . $this->token);

        $this->client->request('GET', '/user/');

        // Vérifier que la réponse a un statut 200
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $responseData = json_decode($this->client->getResponse()->getContent(), true, 512, JSON_THROW_ON_ERROR);
        //Test de la route de suppresion d'un user
        // 403 pour un utilisateur non connecte

        $this->client->request('DELETE', '/user/1');

        // Vérifier que la réponse a un statut 403
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        // 200 pour un user connecte ou admin

        $this->client->request('DELETE', '/user/' . $responseData[0]['id']);

        // Vérifier que la réponse a un statut 200
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
    }

}
