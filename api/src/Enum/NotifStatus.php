<?php

namespace App\Enum;

class NotifStatus
{
    public const Unread = 'Unread';
    public const Read = 'Read';
    public const Archived = 'Archived';
    public const Deleted ='Deleted';
}
