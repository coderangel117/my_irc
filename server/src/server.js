const { createServer } = require("http");
const { Server } = require("socket.io");
const express = require("express");
const app = express();
const path = require("path");
app.use(express.static(path.join(__dirname, "public")));

const httpServer = createServer(app);
const io = new Server(httpServer, {
    cors: {
        origin: "http://localhost:5173",
        methods: ["GET", "POST"],
    }
});

io.on("connection", (socket) => {
    socket.on("enter_room", (room) => {
        socket.join(room);
        socket.on("send_message", (message) => {
            console.log(message)
            const messageWithTimestamp = {
                ...message,
                created_at: new Date().toISOString() // Ajout de la date et heure actuelles
            };
            socket.to(room).emit("receive_message", messageWithTimestamp);
        });

        socket.on("typing", () => {
            socket.to(room).emit("user_typing");
        });

        socket.on("disconnect", () => {
            console.log(`User ${socket.id} disconnected`);
        });
    });

    socket.on("leave_room", (room) => {
        socket.leave(room);
        console.log(socket.rooms);
    });
});

httpServer.listen(3001, () => {
    console.log("Server started on port 3001");
});
