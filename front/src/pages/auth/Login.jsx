import React, {useState} from 'react';
import {accountService} from "../../_services/index.js";
import {Link, useNavigate} from "react-router-dom";


import './auth.css';

const Login = () => {

    let navigate = useNavigate()

    const [credentials, setCredentials] = useState({
        username: '',
        password: '',
    });


    const onChange = (e) => {
        setCredentials(
            {
                ...credentials,
                [e.target.name]: e.target.value
            }
        );
    }

    const SubmitForm = (e) => {
        e.preventDefault();
        accountService.login(credentials)
            .then(data => {
                accountService.saveToken(data.token)
                accountService.profile()
                .then(data => {
                            navigate('/', {replace: true})
                    })
                .catch(error => console.log(error))
                            })
            .catch(error => console.log(error))

    }
    return (
        <form onSubmit={SubmitForm}>
            <div className="group">
                <label htmlFor="username">Identifiant</label>
                <input type="text" name="username" value={credentials.username} onChange={onChange}/>
            </div>
            <div className="group">
                <label htmlFor="password">Mot de passe</label>
                <input type="password" name="password" value={credentials.password} onChange={onChange}/>
            </div>
            <div className="group">
                <button>Connexion</button>
            </div>
            <Link to='/register'>S'enregistrer</Link>
        </form>
    );
};

export default Login;