import Axios from './caller.service'

let login = (credentials) => {
    return Axios
        .post("authenticate", credentials)
        .then((response) => {
            return response.data;
        });
}

let register = (credentials) => {
    return Axios
        .post("register", credentials)
        .then((response) => {
            return response.data;
        });
}

let profile = () => {
    return Axios
        .get("me")
        .then((response) => {
            return response.data;
        });
}

let getToken = () => {
    return localStorage.getItem("token");
}
let decodeToken = (token) => {
    return JSON.parse(atob(token.split(".")[1]));
}

let saveToken = (token) => {
    localStorage.setItem("token", token);
}
let isLoggedIn = () => {
    return !!localStorage.getItem("token");
}
let logout = () => {
    localStorage.removeItem("token");
}
export const accountService = {
    login, saveToken, logout, decodeToken, isLoggedIn, getToken, register, profile
}