<?php

namespace App\Controller;


use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use App\Entity\User;
use App\Enum\UserRole;

class AuthenticationController extends AbstractController
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    #[Route('/authenticate', name: 'app_authentication', methods: ['POST'])]
    public function authenticate(Request $request, UserPasswordHasherInterface $userPasswordHasher, JWTTokenManagerInterface $jwtManager): Response
    {
        // Récupérer les données de la requête
        $requestData = $request->getPayload()->all();
        // Vérifier si le nom d'utilisateur et le mot de passe sont présents
        $username = $requestData['username'] ?? null;
        $password = $requestData['password'] ?? null;

        if (!$username || !$password) {
            return $this->json(['message' => 'Nom d\'utilisateur ou mot de passe manquant'], Response::HTTP_BAD_REQUEST);
        }

        // Trouver l'utilisateur dans la base de données
        $userRepository = $this->entityManager->getRepository(User::class);
        /* @var User $user */
        $user = $userRepository->findOneBy(['username' => $username]);
        if (!$user) {
            return $this->json(['message' => 'Nom d\'utilisateur incorrect'], Response::HTTP_UNAUTHORIZED);
        }

        // Vérifier le mot de passe
        if (!$userPasswordHasher->isPasswordValid($user, $password)) {
            return $this->json(['message' => 'Mot de passe incorrect'], Response::HTTP_UNAUTHORIZED);
        }
        
        // Générer le token JWT
        $token = $jwtManager->create($user);
        return $this->json(['token' => $token]);
    }

    #[Route('/me', name: 'me', methods: ['GET'])]
    public function getUserDetails(): Response
    {
        /* @var User $user */
        $user = $this->getUser();
        if (!$user) {
            return $this->json(['message' => 'JWT invalide ou absent'], Response::HTTP_UNAUTHORIZED);
        }
        
        $userDetails =
            [
                "id" => $user->getId(),
                "username" => $user->getUserIdentifier(),
                "role" => $user->getRole()
            ];
        return $this->json($userDetails, Response::HTTP_OK);
    }


    #[Route('/register', name: 'app_register', methods: ['POST'])]
    public function register(Request $request, UserPasswordHasherInterface $passwordHasher): Response
    {
        $data = $request->getPayload()->all();
        // Vérifier si les champs requis sont présents
        if (!isset($data['username']) || !isset($data['password'])) {
            return $this->json(['error' => 'Missing username or password'], Response::HTTP_BAD_REQUEST);
        }
        $username = $data['username'];
        $password = $data['password'];

        // Vérifier si un utilisateur avec le même nom d'utilisateur existe déjà
        $userRepository = $this->entityManager->getRepository(User::class);
        $userExist = $userRepository->findOneBy(['username' => $username]);
        // Si un utilisateur avec le même nom d'utilisateur existe déjà, renvoie d'une erreur
        if ($userExist) {
            return $this->json(['message' => 'username already exists'], Response::HTTP_CONFLICT);
        }
        
        // Créer un nouvel utilisateur
        $user = new User();
        $user->setUsername($username);
        // Hacher le mot de passe avant de l'enregistrer dans la base de données
        $hashedPassword = $passwordHasher->hashPassword($user, $password);
        $user->setPassword($hashedPassword);
        $user->setRole(UserRole::ROLE_USER);

        // Enregistrer l'utilisateur dans la base de données
        $this->entityManager->persist($user);
        $this->entityManager->flush();
        $role = $user->getRole();

        $userDetails = ["id" => $user->getId(), "username" => $username, "role" => $role];
        return $this->json($userDetails, Response::HTTP_CREATED);
    }
}
