<?php

namespace App\Controller;

use App\Service\BackupService;
use App\Service\RestoreService;
use JsonException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/admin', name: 'backup')]
class AdminController extends AbstractController
{
    private BackupService $backupService;

    public function __construct(BackupService $backupService)
    {
        $this->backupService = $backupService;
    }

    #[Route('/backup', name: 'create_backup', methods: ['POST'])]
    public function createBackup(): Response
    {
        $backupFile = $this->backupService->createBackup();
        return $this->json(['message' => 'Backup created'], Response::HTTP_CREATED);
    }

    #[Route('/backups', name: 'list_backups', methods: ['GET'])]
    public function listBackups(): Response
    {
        $backupDir = $this->getParameter('kernel.project_dir') . '/backups/';
        $files = scandir($backupDir);
        $backups = array_filter($files, static fn($file) => is_file($backupDir . $file));

        return $this->json(['backups' => array_values($backups)], Response::HTTP_OK);
    }

    /**
     * @throws JsonException
     */
    #[Route('/backup/restore', name: 'restore_backup', methods: ['POST'])]
    public function restoreBackup(Request $request, RestoreService $restoreService): Response
    {
        $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
        $backupFilePath = $this->getParameter('kernel.project_dir') . '/backups/' . $data['file'];

        if (!file_exists($backupFilePath)) {
            return $this->json(['error' => 'Backup file not found'], Response::HTTP_NOT_FOUND);
        }

        $restoreService->restoreBackup($backupFilePath);
        return $this->json(['message' => 'Backup restored'], Response::HTTP_OK);
    }
}