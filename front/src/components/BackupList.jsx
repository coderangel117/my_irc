import React, { useState, useEffect } from 'react';
import { AdminService} from "../_services/index.js";
import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper, Button, Container, Typography } from '@mui/material';

const BackupList = () => {
    const [backups, setBackups] = useState([]);

    const fetchBackups = () => {
        AdminService.getListBackups()
            .then(response => {
                if (Array.isArray(response.data.backups)) {
                    setBackups(response.data.backups);
                } else {
                    console.error('API response is not an array:', response.data.backups);
                }
            })
            .catch(error => {
                console.log(error);
            });
    }

    useEffect(() => {
        fetchBackups();
        }, []); 
        
    const createBackup = () => {
        AdminService.createBackup()
            .then(response => {
                fetchBackups();
        })
        .catch(error => {
            console.log(error);
            })
    }
    
    const restoreBackup = (backup) => {
        AdminService.restoreBackup(backup)
            .then(response => {
                setBackups(response.data);
                })
            .catch(error => {
                console.log(error);
    })

    }
    return (
        <Container>
            <Typography variant="h4" gutterBottom>
                Backup Management
            </Typography>
            <Button variant="contained" color="primary" onClick={createBackup}>
                Create Backup
            </Button>
            <TableContainer component={Paper} sx={{ mt: 2 }}>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>Filename</TableCell>
                            <TableCell>Action</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {backups.map((backup) => (
                            <TableRow key={backup}>
                                <TableCell>{backup}</TableCell>
                                <TableCell>
                                    <Button variant="outlined" color="secondary" onClick={() => restoreBackup(backup)}>
                                        Restore
                                    </Button>
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </Container>
        );
}

export default BackupList;