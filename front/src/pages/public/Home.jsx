import Header from "../../components/Header.jsx";
import Navbar from "../../components/Navbar.jsx";

const Home = () => {

    return (
        <div>
            <Header/>
            <Navbar/>
        </div>
    );
};

export default Home;