import React, { useState } from 'react';
import {Container, Grid, Paper, Typography, Box, Toolbar, List, ListItem, ListItemIcon, ListItemText, AppBar, Drawer, IconButton } from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu';
import UserList from '../../components/UserList';
import BackupList from '../../components/BackupList';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import GroupIcon from '@mui/icons-material/Group';
import SettingsIcon from '@mui/icons-material/Settings';
import {Link} from "react-router-dom";
import Header from "../../components/Header.jsx";

const AdminDashboard = () => {
    const [selectedMenuItem, setSelectedMenuItem] = useState('users');

    const renderContent = () => {
        switch (selectedMenuItem) {
            case 'users':
                return <UserList/>;
            case 'backups':
                return <BackupList/>;
            default:
                return <UserList/>;
        }
    };

    const drawer = (
        <div>
            <List>
                <ListItem button onClick={() => setSelectedMenuItem('users')}>
                    <ListItemIcon><AccountCircleIcon/></ListItemIcon>
                    <ListItemText primary="Utilisateurs"/>
                </ListItem>
                <ListItem button onClick={() => setSelectedMenuItem('backups')}>
                    <ListItemIcon><GroupIcon/></ListItemIcon>
                    <ListItemText primary="Sauvegardes"/>
                </ListItem>
                <ListItem>
                    <ListItemIcon><SettingsIcon/></ListItemIcon>
                    <ListItemText primary="Settings"/>
                </ListItem>
            </List>
        </div>
    );


    return (
        <>
            <Header/>
            <Box sx={{display: 'flex'}}>
                <Drawer
                    variant="permanent"
                    sx={{
                        width: 240,
                        flexShrink: 0,
                        [`& .MuiDrawer-paper`]: {width: 240, boxSizing: 'border-box'},
                    }}
                >
                    <Toolbar/>
                    {drawer}
                </Drawer>
                <Box
                    component="main"
                    sx={{flexGrow: 1, p: 2, marginLeft: 10, marginTop: 8}}
                > <Container>
                    <Typography variant="h4" gutterBottom>
                        Gestion des utilisateurs
                    </Typography>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <Paper sx={{padding: 3}}>
                                {renderContent()}
                            </Paper>
                        </Grid>
                    </Grid>
                </Container>
                </Box>
            </Box>
        </>
    );
};

export default AdminDashboard;
