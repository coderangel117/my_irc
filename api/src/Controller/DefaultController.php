<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Attribute\Route;

class DefaultController extends AbstractController
{
    #[Route('/testSuccess', name: 'testSuccess')]
    public function testSuccess():Response
    {
        return new Response('success', Response::HTTP_OK);
    }
    #[Route('/testNotFound', name: 'testNotFound')]
    public function testNotFound():Response
    {
        return new Response('not found', Response::HTTP_NOT_FOUND);
    }
    #[Route('/testError', name: 'testError')]
    public function testError():Response
    {
        return new Response('error', Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}
