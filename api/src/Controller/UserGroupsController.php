<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Groups;
use App\Entity\UserGroups;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/user-groups', name: 'app_user_groups')]
class UserGroupsController extends AbstractController
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    #[Route('/', name: 'app_user_groups')]
    public function index(): Response
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/UserGroupsController.php',
        ]);
    }

    /**
     * @throws \JsonException
     */
    #[Route('', name: 'add_user_to_group', methods: ['POST'])]
    public function addUserToGroup(Request $request): Response
    {
        $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);

        if (!isset($data['user_id']) || !isset($data['groupe_id'])) {
            return $this->json(['error' => 'User ID and Group ID are required'], Response::HTTP_BAD_REQUEST);
        }

        $user = $this->entityManager->getRepository(User::class)->findOneBy(['id' => $data['user_id']]);
        $group = $this->entityManager->getRepository(Groups::class)->findOneBy(['id' => $data['groupe_id']]);

        if (!$user || !$group) {
            return$this->json(['error' => 'User or Group not found'], Response::HTTP_NOT_FOUND);
        }

        if (!$this->getUser())
            return $this->json(['message' => "vous n'êtes pas autorisé à accéder à cette ressource"], Response::HTTP_UNAUTHORIZED);

        $userGroup = new UserGroups();
        $userGroup->setUser($user);
        $userGroup->setGroup($group);

        $this->entityManager->persist($userGroup);
        $this->entityManager->flush();

        return $this->json(['message' => 'User added to group'], Response::HTTP_CREATED);
    }

    /**
     * @throws \JsonException
     */
    #[Route('/{id}', name: 'remove_user_from_group', methods: ['DELETE'])]
    public function removeUserFromGroup(Request $request, int $id): Response
    {
        $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
        $userGroup = $this->entityManager->getRepository(UserGroups::class)->find($id);
        $user = $this->entityManager->getRepository(User::class)->find($data['user_id']);

        if (!$user || !$userGroup) {
            return $this->json(['error' => 'User or Group not found'], Response::HTTP_NOT_FOUND);
        }
        
        if (!$this->getUser())
            return $this->json(['message' => "vous n'êtes pas autorisé à accéder à cette ressource"], Response::HTTP_UNAUTHORIZED);
        
        //if ($this->getUser() !== $user->getUser() && $this->getUser()->getRole() == UserRole::ROLE_USER)
        //    return $this→json(['message' → "vous n'êtes pas autorisé à accéder à cette ressource"], Response::HTTP_FORBIDDEN);
    
        $this->entityManager->remove($userGroup);
        $this->entityManager->flush();
        
        return $this->json(['message' => 'User removed from group'], Response::HTTP_OK);    
    }
}
