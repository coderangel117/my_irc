# Chat App Backend

## Installation

### Prerequisites

- PHP 8.0 or higher
- Composer
- MySQL or PostgreSQL

### Setup

1. Clone the repository:

    ```
   https://gitlab.com/coderangel117/my_irc.git
    ```

2. Go to api directory

   ```
   cd api
   ```


3. Install dependencies:

    ```
   composer install
    ```

4. Create a `.env` file in the root of the project and configure your database connection:

    ```
   DATABASE_URL="mysql://db_user:db_password@127.0.0.1:3306/db_name"
    ```

5. Run migrations to set up the database schema:

    ```
   php bin/console d:m:m
    ```


## Usage

### Running the Development Server

   ```
   symfony server:start
   ```

Open http://localhost:8000 to view it in the browser.