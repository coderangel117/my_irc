<?php

namespace App\Controller;

use App\Entity\FriendRequest;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/friend-request')]
class FriendRequestController extends AbstractController
{
    #[Route('/send/{recipientId}', name: 'send_friend_request', methods: ['POST'])]
    public function sendFriendRequest(int $recipientId, EntityManagerInterface $entityManager): JsonResponse
    {
        $sender = $this->getUser();
        $recipient = $entityManager->getRepository(User::class)->find($recipientId);

        if (!$recipient) {
            return $this->json(['error' => 'Recipient not found'], 404);
        }
        /* @var User $recipient */
        $friendRequest = new FriendRequest();
        $friendRequest->setSender($sender);
        $friendRequest->setRecipient($recipient);

        $entityManager->persist($friendRequest);
        $entityManager->flush();

        return $this->json(['success' => 'Friend request sent'], 200);
    }

    #[Route('/respond/{requestId}', name: 'respond_friend_request', methods: ['POST'])]
    public function respondFriendRequest(int $requestId, Request $request, EntityManagerInterface $entityManager): JsonResponse
    {
        $friendRequest = $entityManager->getRepository(FriendRequest::class)->find($requestId);
        $data = json_decode($request->getContent(), true);

        if (!$friendRequest) {
            return $this->json(['error' => 'Friend request not found'], 404);
        }

        if ($data['response'] === 'accept') {
            $friendRequest->setAccepted(true);
            $sender = $friendRequest->getSender();
            $recipient = $friendRequest->getRecipient();
            $sender->addFriend($recipient);
            $recipient->addFriend($sender);
            $entityManager->persist($sender);
            $entityManager->persist($recipient);

            $entityManager->persist($friendRequest);
        }else{
            $entityManager->remove($friendRequest);
        }
        
        $entityManager->flush();

        return $this->json(['success' => 'Friend request ' . $data['response']], 200);
    }
}
