import { useEffect, useState } from 'react';
import {accountService} from '../_services';
import Header from "./Header.jsx";

const Profile = () => {
    const [userConnected, setUserConnected] = useState({
        id: 0,
        username: '',
        role: ''
    });
    useEffect(() => {
        const fetchData = async () => {
            try {
                const userData = await accountService.profile();
                setUserConnected(userData);

            } catch (error) {
                console.error('Error fetching profile data :', error);
            }
        };

        fetchData();
    }, []);

    return (
        <div>
            <Header/>
            <h1>Mes informations</h1>
            <p>{userConnected.username}</p>
        </div>
    );
};

export default Profile;
