import React from 'react';
import {Link} from "react-router-dom";

const Error = () => {
    return (
        <div>
            Voici la page d'erreur 404

            <Link to={'/'}> Retourner sur la page d'accueil</Link>
        </div>
    );
};

export default Error;