import React from 'react';
import {Link, useNavigate} from "react-router-dom";
import {accountService} from "../_services/index.js";

import "./main.css"

const Header = () => {
    let navigate = useNavigate()

    let Logout = () => {
        accountService.logout()
        navigate("/login")
    }
    return (
        <header>
            <Link to='/'><img src="../../public/vite.svg" alt="logo"/></Link>
            <Link to='/profile'>Page de profil</Link>
            <button style={{width: "100px"}} onClick={Logout}>se déconnecter</button>

        </header>
    );
};

export default Header;