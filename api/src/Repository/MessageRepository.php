<?php

namespace App\Repository;

use App\Entity\Message;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Message>
 */
class MessageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Message::class);
    }



/**
     * Find messages between the connected user and another specified user.
     *
     * @param int $userId1
     * @param int $userId2
     * @return Message[]
     */
    public function findMessagesBetweenUsers(int $userId1, int $userId2): array
    {
        $qb = $this->createQueryBuilder('m')
            ->where('(m.sender = :user1 AND m.recipient = :user2) OR (m.sender = :user2 AND m.recipient = :user1)')
            ->setParameter('user1', $userId1)
            ->setParameter('user2', $userId2)
            ->orderBy('m.created_at', 'ASC');

        return $qb->getQuery()->getResult();
    }

    /**
     * Find users connected with the current one
     *
     * @param User $user
     * @return array
     */
    public function findUsersInConversationsWith(User $user): array
    {
        $qb1 = $this->createQueryBuilder('m')
            ->select('DISTINCT u.id, u.username')
            ->join('m.sender', 'u')
            ->where('m.recipient = :user')
            ->setParameter('user', $user);

        $qb2 = $this->createQueryBuilder('m2')
            ->select('DISTINCT u2.id, u2.username')
            ->join('m2.recipient', 'u2')
            ->where('m2.sender = :user')
            ->setParameter('user', $user);

        $results1 = $qb1->getQuery()->getResult();
        $results2 = $qb2->getQuery()->getResult();

        // Combine results and remove duplicates
        $combinedResults = array_merge($results1, $results2);

        $uniqueResults = [];
        foreach ($combinedResults as $result) {
            $uniqueResults[$result['id']] = $result;
        }

        return array_values($uniqueResults);
    }
    //    /**
    //     * @return Message[] Returns an array of Message objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('m')
    //            ->andWhere('m.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('m.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?Message
    //    {
    //        return $this->createQueryBuilder('m')
    //            ->andWhere('m.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
