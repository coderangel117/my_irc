import Axios from './caller.service'

let createBackup = () => {
    return Axios.post('/admin/backup')
        .then((response) => {
            return response
        })
}
let restoreBackup = (data) => {
    return Axios.post('/admin/restore', data)
    .then((response) => {
        return response
        })
}
let getListBackups = () => {
    return Axios.get('/admin/backups')
            .then((response) => {
                return response
                })
}
export const AdminService = {
    createBackup,
    restoreBackup,
    getListBackups
    }