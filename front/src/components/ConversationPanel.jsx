import PropTypes from 'prop-types';
import { useEffect, useRef, useState } from 'react';
import { accountService, messageService } from '../_services/index.js';
import io from 'socket.io-client';
import './ConversationPanel.css';

const socket = io("http://localhost:3001");

const ConversationPanel = ({ user }) => {

    const token = accountService.getToken();
    const [messages, setMessages] = useState([]);
    const [userConnected, setUserConnected] = useState({ id: 0, username: '', role: '' });
    const [sentMessage, setSentMessages] = useState({
        content: '',
        group: null,
        media: null,
        sender: accountService.decodeToken(token).sub,
        recipient: user.username
    });
    const messagesEndRef = useRef(null);

    const fetchMessages = async () => {
        try {
            const userData = await accountService.profile();
            setUserConnected(userData);
            const conversation = await messageService.getConversationWithUser(user.id);
            if (Array.isArray(conversation)) {
                setMessages(conversation);
            } else {
                console.error('Expected an array but got:', conversation);
            }
        } catch (error) {
            console.error('Error fetching conversations:', error);
        }
    };

    useEffect(() => {
        if (user.id && userConnected.id) {
            const room = [user.id, userConnected.id].sort().join('_');
            socket.emit('enter_room', room);

            socket.on('receive_message', (message) => {
                console.log(message)
                setMessages((prevMessages) => [...prevMessages, message]);
            });

            return () => {
                socket.emit('leave_room', room);
            };
        }
    }, [user.id, userConnected.id]);

    useEffect( () => {
        fetchMessages();
    }, [user]);

    useEffect(() => {
        messagesEndRef.current?.scrollIntoView({ behavior: 'smooth' });
    }, [messages]);

    const sendMessage = async (e) => {
        e.preventDefault();

        try {

            const response = await messageService.sendMessage({sentMessage});
            console.log('id du nouveau message:', response.idmessage);
            const messageData = {
                ...sentMessage,
                id: response.idmessage,
                sender_username: userConnected.username,
                created_at: {
                    date: new Date()
                }
            };
            socket.emit('send_message', messageData);
            setSentMessages({ ...sentMessage, content: '' });
            fetchMessages();
        } catch (error) {
            console.error('Error sending message:', error);
        }
    };

    const onChange = (e) => {
        setSentMessages({
            ...sentMessage,
            content: e.target.value,
            sender: userConnected.id,
            recipient: user.id
        });
    };


    const formatTime = (timestamp) => {
        return new Date(timestamp).toLocaleTimeString([], {hour: '2-digit', minute: '2-digit'});
    };

    const formatDate = (timestamp) => {
        return new Date(timestamp).toLocaleDateString();
    };

    const renderMessages = () => {
        const messageGroups = messages.reduce((acc, message) => {
            const date = formatDate(message.created_at.date || message.created_at);
            if (!acc[date]) acc[date] = [];
            acc[date].push(message);
            return acc;
        }, {});

        return Object.keys(messageGroups).map((date) => (
            <div key={date}>
                <div className="date-separator">{date}</div>
                {messageGroups[date].map((message) => (

                    <div className="message" key={message.id}>
                        <div className="message-content">
                            <div className="message-header">
                                <span className="message-username">{message.sender_username != null ? message.sender_username : message.sender }</span>
                                <span className="message-timestamp">{formatTime(message.created_at.date || message.created_at) }</span>
                            </div>
                            <div className="message-text">{message.content}</div>
                        </div>
                    </div>
                ))}
            </div>
        ));
    };

    return (
        <div className="conversation-panel">
            <div className="messages-container">
                {renderMessages()}
                <div ref={messagesEndRef}/>
            </div>
            <form className="send-message-form" onSubmit={sendMessage}>
                <input
                    className="send-message-input"
                    name='content'
                    placeholder='Type your message'
                    type='text'
                    value={sentMessage.content}
                    onChange={onChange}
                />
                <button className="send-message-button" type="submit">
                    Envoyer
                </button>
            </form>
        </div>
    );
};

ConversationPanel.propTypes = {
    user: PropTypes.object.isRequired,
};

export default ConversationPanel;
