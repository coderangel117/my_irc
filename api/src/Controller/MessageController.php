<?php

namespace App\Controller;

use App\Entity\Message;
use App\Entity\User;
use App\Entity\Groups;
use App\Enum\UserRole;
use App\Entity\Media;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route(path: '/message')]
class MessageController extends AbstractController
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    #[Route('/', name: 'user_message', methods: ['GET'])]
    public function index(): Response
    {
        $user = $this->getUser();
        if ($user->getRole() === UserRole::ROLE_ADMIN) {
            $messages = $this->entityManager->getRepository(Message::class)->findAll();

        } else {
            $queryBuilder = $this->entityManager->getRepository(Message::class)->createQueryBuilder('m');
            $queryBuilder->where('m.sender = :user')
                ->orWhere('m.recipient = :user')
                ->setParameter('user', $user);

            $messages = $queryBuilder->getQuery()->getResult();
        }

        $Messages = [];
        foreach ($messages as $m) {
            /** @var Message $m */
            $Messages[] = [
                'sender' => $m->getSender()->getUserIdentifier(),
                'recipient' => $m->getRecipient()->getUserIdentifier(),
                'content' => $m->getContent(),
//                'group' => $m->getGroup()->getId(),
//                'media' => $m->getMedia()->getId(),
                'created_at' => $m->getCreatedAt()
            ];
        }

        return $this->json($Messages, Response::HTTP_OK, [], ['groups' => 'messages:read']);
    }

    #[Route(path:'/user/{userId}', name:"messages_with_user", methods:['GET'])]
    public function getConversationWithUser(int $userId): JsonResponse
    {
        $loggedInUser = $this->getUser();
        if (!$loggedInUser) {
            return $this->json(['error' => 'User not logged in'], 403);
        }
        $messageRepository = $this->entityManager->getRepository(Message::class);
        $Messages = [];
        $messages = $messageRepository->findMessagesBetweenUsers($loggedInUser->getId(), $userId);
        foreach ($messages as $m) {
            /** @var Message $m */
            $Messages[] = [
                'id' => $m->getId(),
                'sender' => $m->getSender()->getUserIdentifier(),
                'recipient' => $m->getRecipient()->getUserIdentifier(),
                'content' => $m->getContent(),
                'group' => $m->getGroup()?->getId(),
                'media' => $m->getMedia()?->getId(),
                'created_at' => $m->getCreatedAt()
            ];
        }
        return $this->json($Messages, Response::HTTP_OK, [], ['groups' => 'messages:read']);
    }



    #[Route('/{userId}', name: 'start_conversation', methods: ['POST'])]
    public function startConversation(int $userId, EntityManagerInterface $entityManager): JsonResponse
    {
        $loggedInUser = $this->getUser();
        $recipient = $entityManager->getRepository(User::class)->find($userId);

        if (!$recipient) {
            return $this->json(['error' => 'User not found'], 404);
        }

        if (!$loggedInUser->getFriends()->contains($recipient)) {
            return $this->json(['error' => 'You can only start a conversation with a friend'], Response::HTTP_FORBIDDEN);
        }

        // Logic to start a conversation (if necessary) and send the first message

        return $this->json(['success' => 'Conversation started'], Response::HTTP_OK);
    }


    /**
     * @throws \JsonException
     */
    #[Route('', name: 'create_message', methods: ['POST'])]
    public function createMessage(Request $request, ValidatorInterface $validator): Response
    {

        $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);

        // Validate and retrieve the sender
        $userRepository = $this->entityManager->getRepository(User::class);
        $sender = $userRepository->findOneBy(['id' => $this->getUser()->getId()]);
        if (!$sender) {
            return $this->json(['error' => 'Invalid sender ID'], Response::HTTP_BAD_REQUEST);
        }

        // Validate and retrieve the group
        $groupRepository = $this->entityManager->getRepository(Groups::class);
        $group = isset($data['group']) ? $groupRepository->find($data['group']) : null;
        if ($group === null && isset($data['group'])) {
            return $this->json(['error' => 'Invalid group ID'], Response::HTTP_BAD_REQUEST);
        }

        // Validate and retrieve the media (optional)
        $mediaRepository = $this->entityManager->getRepository(Media::class);

        $media = isset($data['media']) ? $mediaRepository->find($data['media']) : null;
        if ($media === null && isset($data['media'])) {
            return $this->json(['error' => 'Invalid media ID'], Response::HTTP_BAD_REQUEST);
        }

        // Validate and retrieve the recipient (optional)
        $recipient = isset($data['recipient']) ? $userRepository->findOneBy(['id' => $data['recipient']]) : null;
        //return $this->json($recipient->getId());
        if ($recipient === null && isset($data['id'])) {
            return $this->json(['error' => 'Invalid recipient ID'], Response::HTTP_BAD_REQUEST);
        }

        // Create the new message
        $message = new Message();
        $message->setSender($sender);
        $message->setGroup($group);
        $message->setMedia($media);
        $message->setRecipient($recipient);
        $message->setContent($data['content']);

        // Validate the message entity
        $errors = $validator->validate($message);
        if (count($errors) > 0) {
            $errorMessages = [];
            foreach ($errors as $error) {
                $errorMessages[] = $error->getMessage();
            }
            return $this->json(['errors' => $errorMessages], Response::HTTP_BAD_REQUEST);
        }

        // Persist the new message to the database
        $this->entityManager->persist($message);
        $this->entityManager->flush();

        // Return the created message as a response
        $messages = [
            "idmessage" => $message->getId(),
            "content" => $message->getContent(),
            "recipient" => $message->getRecipient()->getUserIdentifier()
        ];

        return $this->json($messages, Response::HTTP_CREATED);
    }

    /**
     * @throws \JsonException
     */
    #[Route('/{id}', name: 'update_message', methods: ['PUT'])]
    public function update($id, Request $request, ValidatorInterface $validator): Response
    {
        $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
        $message = $this->entityManager->getRepository(Message::class)->find($id);
        if ($message === null) {
            return $this->json(['error' => 'Invalid message ID'], Response::HTTP_BAD_REQUEST);
        }
        if (!$this->getUser()) {
            return $this->json(['message' => "vous n'êtes pas autorisé à accéder à cette ressource"], Response::HTTP_UNAUTHORIZED);
        }

        if ($this->getUser() !== $message->getSender() && $this->getUser()->getRole() == UserRole::ROLE_USER) {
            return $this->json(['message' => "vous n'êtes pas autorisé à accéder à cette ressource"], Response::HTTP_FORBIDDEN);
        }

        // Validate the message entity
        $errors = $validator->validate($message);
        if (count($errors) > 0) {
            $errorMessages = [];
            foreach ($errors as $error) {
                $errorMessages[] = $error->getMessage();
            }
            return $this->json(['errors' => $errorMessages], Response::HTTP_BAD_REQUEST);
        }

        // Update the message entity
        $message->setContent($data['content']);
        $this->entityManager->persist($message);
        $this->entityManager->flush();

        // Return the updated message as a response
        $messages = ["content" => $message->getContent()];
        return $this->json($messages, Response::HTTP_OK);

    }

    #[Route('/{id}', name: 'delete_message', methods: ['DELETE'])]
    public function deleteMessage($id): JsonResponse
    {
        $message = $this->entityManager->getRepository(Message::class)->find($id);
        if ($message === null) {
            return $this->json(['error' => 'Invalid message ID'], Response::HTTP_BAD_REQUEST);
        }
        if (!$this->getUser()) {
            return $this->json(['message' => "vous n'êtes pas autorisé à accéder à cette ressource"], Response::HTTP_UNAUTHORIZED);
        }

        if ($this->getUser() !== $message->getSender() && $this->getUser()->getRole() == UserRole::ROLE_USER) {
            return $this->json(['message' => "vous n'êtes pas autorisé à accéder à cette ressource"], Response::HTTP_FORBIDDEN);
        }


        $this->entityManager->remove($message);
        $this->entityManager->flush();
        return $this->json(['message' => 'Message deleted'], Response::HTTP_OK);
    }


    #[Route('/conversations', name: 'get_user_conversations', methods: ['GET'])]
    public function getUserConversations(): JsonResponse
    {
        /* @var User $user */
        // Récupère l'utilisateur connecté
        $user = $this->getUser();

        // Récupère les utilisateurs avec lesquels l'utilisateur connecté à des conversations
        $users = $this->entityManager->getRepository(Message::class)->findUsersInConversationsWith($user);
       
        return $this->json($users, Response::HTTP_OK);
    }
}
