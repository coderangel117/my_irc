import React, {useState} from 'react';

const SearchBar = () => {

    const [searchTerm, setSearchTerm] = useState('');

    return (

        <div>
            Rechercher un utilisateur
            <br/>
            {searchTerm}
            <input type="text" value={searchTerm} onChange={(e) => setSearchTerm(e.target.value)}/>
        </div>
    );
};

export default SearchBar;