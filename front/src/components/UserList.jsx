import {useEffect} from 'react';
import {userService} from "../_services/index.js";
import {useUserContext} from '../_helpers/UserContext';
import {Link} from 'react-router-dom';
import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper, IconButton, Typography, Link as MuiLink } from '@mui/material';
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';



const UserList = () => {

    const {users, setUsers} = useUserContext();

    useEffect(() => {
        userService.users()
            .then((response) => {
                setUsers(response.data);
            })
            .catch(error => console.error(error.response.data.message));
    }, [setUsers]);

    return (
        <TableContainer component={Paper} variant="outlined">
            <Typography variant="h6" gutterBottom component="div" sx={{ padding: 2 }}>
                User List
            </Typography>
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell>Username</TableCell>
                        <TableCell align="center">Détails</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {users.map((user) => (
                        <TableRow key={user.id}>
                            <TableCell>
                            {user.username}
                            </TableCell>
                            <TableCell align="right">
                                <MuiLink component={Link} to={`/user/${user.id}`} color="primary">
                                <IconButton color="primary">
                                        <ArrowForwardIosIcon />
                                    </IconButton>
                                </MuiLink>
                            </TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
};

export default UserList;