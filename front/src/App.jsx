import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Login from './pages/auth/Login';
import Register from './pages/auth/Register';
import AdminDashboard from './pages/admin/AdminDashboard'
import Error from './_utils/Error';
import Home from './pages/public/Home';
import UserDetail from './components/UserDetail';
import { UserProvider } from './_helpers/UserContext';
import Profile from './components/Profile';
import ProtectedRoute from './_utils/ProtectedRoute.jsx';
import PublicRoute from './_utils/PublicRoute.jsx';


function App() {
    return (
        <UserProvider>
            <BrowserRouter>
                <Routes>
                    <Route
                        path="/"
                        element={
                            <ProtectedRoute>
                                <Home/>
                            </ProtectedRoute>
                        }
                        />
                    <Route
                        path="/login"
                        element={
                            <PublicRoute>
                                <Login />
                            </PublicRoute>
                        }
                    />
                    <Route
                        path="/register"
                        element={
                            <PublicRoute>
                                <Register />
                            </PublicRoute>
                        }
                    />
                    <Route
                    path='/admin'
                    element={
                        <ProtectedRoute>
                            <AdminDashboard />
                            </ProtectedRoute>
                        }
                        />
                    <Route
                        path="/profile"
                        element={
                            <ProtectedRoute>
                                <Profile />
                            </ProtectedRoute>
                        }
                    />
                    <Route
                        path="/user/:userId"
                        element={
                            <ProtectedRoute>
                                <UserDetail />
                            </ProtectedRoute>
                        }
                    />
                    <Route path="*" element={<Error />} />
                </Routes>
            </BrowserRouter>
        </UserProvider>
    );
}

export default App;
