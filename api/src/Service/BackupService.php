<?php

namespace App\Service;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpKernel\KernelInterface;

class BackupService
{
    private KernelInterface $kernel;
    private Filesystem $filesystem;

    public function __construct(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
        $this->filesystem = new Filesystem();
    }

    public function createBackup(): string
    {
        $backupDir = $this->kernel->getProjectDir() . '/backups/';
        if (!$this->filesystem->exists($backupDir)) {
            $this->filesystem->mkdir($backupDir);
        }

        $filename = $backupDir . 'backup_' . date('YmdHis') . '.sql';
        $command = 'mysqldump --user=%s --password=%s --host=%s %s > %s';

        $databaseUrl = parse_url($_ENV['DATABASE_URL']);
        $command = sprintf(
            $command,
            $databaseUrl['user'],
            $databaseUrl['pass'],
            $databaseUrl['host'],
            substr($databaseUrl['path'], 1),
            $filename
        );

        exec($command);

        return $filename;
    }
}
