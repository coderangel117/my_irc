<?php

namespace App\Controller;

use App\Entity\Groups;
use App\Enum\UserRole;
use Doctrine\ORM\EntityManagerInterface;
use JsonException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route('/groups', name: 'app_groups')]
class GroupsController extends AbstractController
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    #[Route('/', name: 'list_groups', methods: ['GET'])]
    public function index(): Response
    {
        $user = $this->getUser();
        if ($user->getRole() === UserRole::ROLE_ADMIN) {
            $groups = $this->entityManager->getRepository(Groups::class)->findAll();

        } else {
            return $this->json(['message' => "vous n'êtes pas autorisé à accéder à cette ressource"], Response::HTTP_FORBIDDEN);
        }
        $Groups = [];
        foreach ($groups as $group) {
            $Groups[] = [
                'id' => $group->getId(),
                'group_name' => $group->getGroupName()
            ];
        }
        
        return $this->json($Groups);
    }

    #[Route('', name: 'create_group', methods: ['POST'])]
    public function createGroup(Request $request, ValidatorInterface $validator): Response
    {
        $data = $request->getPayload()->all();

        $user = $this->getUser();
        if (!$user) {
            return $this->json(['message' => "vous n'êtes pas autorisé à accéder à cette ressource"], Response::HTTP_UNAUTHORIZED);
        }

        $group = new Groups();
        $group->setGroupName($data['group_name']);

        $errors = $validator->validate($group);
        if (count($errors) > 0) {   
            return $this->json(['errors' => $errors], Response::HTTP_BAD_REQUEST);
        }

        $this->entityManager->persist($group);
        $this->entityManager->flush();
        
        return $this->json(['message' => 'Groupe créé avec succès'], Response::HTTP_CREATED);
    }

    #[Route('/{id}', name: 'get_group', methods: ['GET'])]
    public function getGroup($id): Response
    {
        $group = $this->entityManager->getRepository(Groups::class)->find($id);
        if (!$group) {
            return $this->json(['error' => 'Group not found'], Response::HTTP_NOT_FOUND);
        }

        $user = $this->getUser();
        if (!$user) {
            return $this->json(['message' => "vous n'êtes pas autorisé à accéder à cette ressource"], Response::HTTP_UNAUTHORIZED);
        }


        return $this->json(['group_name' => $group->getGroupName()], Response::HTTP_OK);
    }

    /**
     * @throws JsonException
     */
    #[Route('/{id}', name: 'update_group', methods: ['PUT'])]
    public function UpdateGroup($id, Request $request, ValidatorInterface $validator): Response
    {
        $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);

        $group = $this->entityManager->getRepository(Groups::class)->find($id);
        if (!$group) {
            return $this->json(['error' => 'Group not found'], Response::HTTP_NOT_FOUND);
        }

        $user = $this->getUser();
        if (!$user) {
            return $this->json(['message' => "vous n'êtes pas autorisé à accéder à cette ressource"], Response::HTTP_UNAUTHORIZED);
        }

        $group->setGroupName($data['group_name']);

        $errors = $validator->validate($group);
        if (count($errors) > 0) {
            $errorMessages = [];
            foreach ($errors as $error) {
                $errorMessages[] = $error->getMessage();
            }
            return $this->json(['errors' => $errorMessages], Response::HTTP_BAD_REQUEST);
        }

        $this->entityManager->persist($group);
        $this->entityManager->flush();

        $updatedgroupe = [ 'name' => $group->getGroupName()];

        return $this->json($updatedgroupe, Response::HTTP_OK);
    }

    #[Route('/{id}', name: 'delete_user', methods: ['DELETE'])]
    public function deleteGroup($id): Response
    {
        $group = $this->entityManager->getRepository(Groups::class)->find($id);
        if (!$group) {
            return $this->json(['error' => 'Group not found'], Response::HTTP_NOT_FOUND);
            }
    
        $user = $this->getUser();
        if (!$user) {
            return $this->json(['message' => "vous n'êtes pas autorisé à accéder à cette ressource"], Response::HTTP_UNAUTHORIZED);
        }
    
        $this->entityManager->remove($group);
        $this->entityManager->flush();
    
        return $this->json(['message' => 'Group deleted'], Response::HTTP_OK);
    }
}
