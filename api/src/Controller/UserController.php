<?php

namespace App\Controller;

use App\Entity\User;
use App\Enum\UserRole;
use Doctrine\ORM\EntityManagerInterface;
use JsonException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route(path: '/user')]
class UserController extends AbstractController
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    #[Route(path: '/', name: 'app_user', methods: ['GET'])]
    public function getUsers(): Response
    {

        if (!$this->getUser()){
            return $this->json("Vous devez vous connecter pour accéder à cette page.", Response::HTTP_FORBIDDEN);
        }
        $userRepository = $this->entityManager->getRepository(User::class);
        if ($this->getUser()->getRole() === UserRole::ROLE_ADMIN) {
            $users = $userRepository->findAll();
        } else {
            $users = $userRepository->findBy(['id' => $this->getUser()->getId()]);
        }
        $usersDetails = [];
        foreach ($users as $user) {
            /** @var User $user */
            $usersDetails[] = [
                "id" => $user->getId(),
                "username" => $user->getUsername(),
                "role" => $user->getRole(),
            ];
        }

        return $this->json($usersDetails, Response::HTTP_OK, [], ['groups' => 'user:read']);
    }

    #[Route(path: "/{id}", name: "get_user", methods: ["GET"])]
    public function getUserDetails(int $id): JsonResponse
    {
        $user = $this->entityManager->getRepository(User::class)->findOneBy(['id' => $id]);

        if (!$user) {
            return $this->json(['message' => "Aucun utilisateur avec l'id $id"], Response::HTTP_NOT_FOUND);
        }

        return $this->json(
            [
                "id" => $user->getId(),
                "username" => $user->getUsername(),
                "role" => $user->getRole(),
            ], Response::HTTP_OK);
    }

    /**
     * @throws JsonException
     */
    #[Route("/{id}", name: "modify_user", methods: ["PUT"])]
    public function modifyUser(Request $request, $id): JsonResponse
    {
        $user = $this->entityManager->getRepository(User::class)->findOneBy(['id' => $id]);

        $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
        if (!$user) {
            return $this->json(['message' => 'Utilisateur inexistant.'], Response::HTTP_NOT_FOUND);
        }

        if ($this->getUser()->getId() === $user->getId() || $this->getUser()->getRole() === UserRole::ROLE_ADMIN) {
            foreach ($data as $key => $value) {
                switch ($key) {
                    case "username":
                        $user->setUsername($value);
                        break;
                    case "role":
                        $user->setRole($value);
                        break;
                    default :
                        return $this->json(['message' => "Aucune modification réalisée."], Response::HTTP_BAD_REQUEST);
                }
            }
            //Mise à jour de la base de données
            $this->entityManager->persist($user);
            $this->entityManager->flush();
        } else {
            return $this->json("Vous n'avez pas les droits pour modifier cet utilisateur.", Response::HTTP_FORBIDDEN);
        }

        return $this->json([
            "id" => $user->getId(),
            "username" => $user->getUsername(),
            "roles" => $user->getRoles()
        ], Response::HTTP_OK);
    }

    #[Route(path: '/{id}', name: "delete_user", methods: ["DELETE"])]
    public function deleteUser(int $id): JsonResponse
    {
        $user = $this->entityManager->getRepository(User::class)->findOneBy(['id' => $id]);

        if (!$user) {
            return $this->json("L'utilisateur n'existe pas.", Response::HTTP_NOT_FOUND);
        }

        if ($this->getUser()->getId() !== $user->getId() || $this->getUser()->getRoles() == UserRole::ROLE_USER) {
            return $this->json("Vous n'avez pas les droits pour supprimer cet utilisateur.", Response::HTTP_FORBIDDEN);
        }
        //Suppression de l'utilisateur de la BDD
        $this->entityManager->remove($user);
        $this->entityManager->flush();
        return $this->json("L'utilisateur a bien été supprimé.", Response::HTTP_OK);

    }
}
