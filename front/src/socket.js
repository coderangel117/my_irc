import { io } from 'socket.io-client';


const Socket = io("http://localhost:3001", {
    withCredentials: true,
    transports: ['websocket', 'polling'],
    cors: {
        origin: "http://localhost:3001",
        methods: ["GET", "POST"],
    }
});
export const socket = io(URL);