import Axios from './caller.service'

let addresses = () => {
    return Axios
        .get("address")
        .then((response) => {
            return response.data;
        })
}
let addressUpdate = (id, data) => {
    return Axios.put(`/address/${id}`, data)
        .then((response) => {
            return response
        })
}

let addressDelete = (id) => {
    return Axios.delete(`/address/${id}`)

        .then((response) => {
            return response
        })
}

export const addressService = {
    addresses,    addressUpdate,
    addressDelete
}