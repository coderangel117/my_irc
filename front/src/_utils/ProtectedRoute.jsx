import { Navigate } from 'react-router-dom';
import { accountService } from '../_services';

const ProtectedRoute = ({ children }) => { 
    const isLoggedIn = accountService.isLoggedIn();

    if (!isLoggedIn) {
        return <Navigate to="/login" replace />;
    }/*
    accountService.profile()
    .then(data=>{
        if(data.role == 'ROLE_ADMIN')
            {
                return <Navigate to='/admin' replace />
            }
    })*/
   
    return children;
};

export default ProtectedRoute;
