import {useState} from 'react';
import {useNavigate, useParams} from 'react-router-dom';
import {userService} from "../_services/index.js";
import {useUserContext} from '../_helpers/UserContext';
import { Button, TextField, Typography, Card, CardContent, CardActions, Container, Box } from '@mui/material';


function UserDetail() {

    let navigate = useNavigate()

    const {userId} = useParams();
    const {users, setUsers} = useUserContext();
    const userDetail = users.find(user => user.id === parseInt(userId));
    const [isEditing, setIsEditing] = useState(false);
        
    const [error, setError] = useState(null);
    const [state, setState] = useState({
        userName: ""
    })

    if (!userDetail) {
        return <div>User not found</div>;
    }
    const handleChange = (e) => {
        const {id, value} = e.target
        setState(prevState => ({
            ...prevState,
            [id]: value
        }))
    }

    const handleEditUser = (e) => {
        e.preventDefault();

        userService.userUpdate(userId, {"username": state.userName})
            .then((response) => {
                setUsers(prevUsers => prevUsers.map(user => user.id === userId ? response.data : user));
                setIsEditing(false);
            })
            .catch(error => {
                setError(error.response.data.message);
                console.error(error.response.data.message)
            });
    }
    const handleDeleteUser = () => {
        userService.userDelete(userId)
            .then(() => {
                setUsers(prevUsers => prevUsers.filter(user => user.id !== userId));
                navigate('/', {replace: true})
            })
            .catch(error => {
                setError(error.response.data.message);
                console.error(error.response.data.message);
            });
    };

   /* const handleAddress = (ev) => {
        const {id, value} = ev.target
        setEditedAddress(prevState => ({
            ...prevState,
            [id]: value
        }))
    };


    const handleEditAddress = (adId) => {
        setIsEditingAddress(true);
        setEditedAddress(prevState => ({...prevState, id: adId}));
    }

    const handleSaveAddress = (e) => {
        e.preventDefault();
        addressService.addressUpdate(editedAddress.id, editedAddress)
            .then((response) => {
                setUsers(prevUsers => prevUsers.map(user => {
                    if (user.id === userId) {
                        return {
                            ...user,
                            addresses: user.addresses.map(ad => ad.id === editedAddress.id ? response.data : ad)
                        };
                    }
                    return user;
                }));
                setIsEditingAddress(false);
                setEditedAddress(null);
            })
            .catch(error => {
                setError(error.response.data.message);
                console.error(error.response.data.message);
            });
    };

    const handleDeleteAddress = (idAd) => {
        addressService.addressDelete(idAd)
            .then(() => {
                setUsers(prevUsers => prevUsers.map(user => {
                    if (user.id === userId) {
                        return {
                            ...user,
                            addresses: user.addresses.filter(ad => ad.id !== idAd)
                        };
                    }
                    return user;
                }));
            })
            .catch(error => {
                setError(error.response.data.message);
                console.error(error.response.data.message);
            });
    };*/

    return (
        <Container maxWidth="sm" sx={{ mt: 4 }}>
            <Card>
                <CardContent>
                    <Typography variant="h5" component="div">
                        User Detail
                    </Typography>
                    {isEditing ? (
                        <Box component="form" sx={{ mt: 2 }}>
                            <TextField
                                fullWidth
                                id="userName"
                                label="Username"
                                value={state.userName}
                                onChange={handleChange}
                                variant="outlined"
                                margin="normal"
                            />
                        </Box>
                    ) : (
                        <Typography variant="body1" sx={{ mt: 2 }}>
                            <strong>Username:</strong> {userDetail.username}
                        </Typography>
                    )}
                </CardContent>
                <CardActions>
                    {isEditing ? (
                        <>
                            <Button variant="contained" color="primary" onClick={handleEditUser}>
                                Save
                            </Button>
                            <Button variant="outlined" color="secondary" onClick={() => setIsEditing(false)}>
                                Cancel
                            </Button>
                        </>
                    ) : (
                        <>
                            <Button variant="contained" color="primary" onClick={() => setIsEditing(true)}>
                                Edit User
                            </Button>
                            <Button variant="outlined" color="secondary" onClick={handleDeleteUser}>
                                Delete User
                            </Button>
                        </>
                    )}
                </CardActions>
            </Card>
        </Container>
    )
}

export default UserDetail