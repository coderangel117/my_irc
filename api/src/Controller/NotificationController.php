<?php

namespace App\Controller;

use App\Entity\Notification;
use App\Entity\User;
use App\Entity\Groups;
use App\Entity\Message;
use App\Enum\UserRole;
use App\Enum\NotifStatus;
use App\Repository\NotificationRepository;
use Doctrine\ORM\EntityManagerInterface;
use JsonException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/notification', name: 'app_notification')]
class NotificationController extends AbstractController
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @throws JsonException
     */
    #[Route('', name: 'create_notification', methods: ['POST'])]
    public function create(Request $request): Response
    {
        $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);

        $notification = new Notification();
        $notification->setStatus($data['status']?? NotifStatus::Unread);

        // Assume you have repositories for User, Groups, and Message
        $user = $this->entityManager->getRepository(User::class)->find($data['user_id']);
        if (!$user) {
            return $this->json(['error' => 'Invalid user ID'], Response::HTTP_BAD_REQUEST);
        }

        $group = $this->entityManager->getRepository(Groups::class)->find($data['group_id']);
        if (!$group) {
            return $this->json(['error' => 'Invalid group ID'], Response::HTTP_BAD_REQUEST);
        }

        $message = $this->entityManager->getRepository(Message::class)->find($data['message_id']);
        if (!$message) {
            return $this->json(['error' => 'Invalid message ID'], Response::HTTP_BAD_REQUEST);
        }

        if (!$this->getUser())
            return $this->json(['message' => "vous n'êtes pas autorisé à accéder à cette ressource"], Response::HTTP_UNAUTHORIZED);

        if ($this->getUser() !== $user && $this->getUser()->getRole() === UserRole::ROLE_USER)
            return $this->json(['message' => "vous n'êtes pas autorisé à accéder à cette ressource"], Response::HTTP_FORBIDDEN);


        $notification->setUser($user);
        $notification->setGroup($group);
        $notification->setMessage($message);

        $this->entityManager->persist($notification);
        $this->entityManager->flush();

        $result = ['status'=> $notification->getStatus()];

        return $this->json($result, Response::HTTP_CREATED);
    }

    #[Route('/{id}', name: 'get_notification', methods: ['GET'])]
    public function readNotification($id, NotificationRepository $notificationRepository): Response
    {
        $notification = $notificationRepository->find($id);
        if (!$notification) {
            return $this->json(['error' => 'Notification not found'], Response::HTTP_NOT_FOUND);
        }

        if (!$this->getUser())
            return $this->json(['message' => "vous n'êtes pas autorisé à accéder à cette ressource"], Response::HTTP_UNAUTHORIZED);

        $result = ['status'=> $notification->getStatus()];

        return $this->json($result, Response::HTTP_OK);
    }

    /**
     * @throws JsonException
     */
    #[Route('/{id}', name: 'update_notification', methods: ['PUT'])]
    public function update($id, Request $request, NotificationRepository $notificationRepository): Response
    {
        $notification = $notificationRepository->find($id);
        if (!$notification) {
            return $this->json(['error' => 'Notification not found'], Response::HTTP_NOT_FOUND);
        }

        $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);

        if (!$this->getUser()) {
            return $this->json(['message' => "vous n'êtes pas autorisé à accéder à cette ressource"], Response::HTTP_UNAUTHORIZED);
        }
        if (isset($data['status'])) {
            $notification->setStatus($data['status']);
        }

        if (isset($data['user_id'])) {
            $user = $this->entityManager->getRepository(User::class)->find($data['user_id']);
            $notification->setUser($user);
        }

        if (isset($data['group_id'])) {
            $group = $this->entityManager->getRepository(Groups::class)->find($data['group_id']);
            $notification->setGroup($group);
        }

        if (isset($data['message_id'])) {
            $message = $this->entityManager->getRepository(Message::class)->find($data['message_id']);
            $notification->setMessage($message);
        }

        $this->entityManager->flush();
        return $this->json($notification);
    }

    #[Route('/{id}', name: 'delete_notification', methods: ['DELETE'])]
    public function delete($id, NotificationRepository $notificationRepository): Response
    {
        $notification = $notificationRepository->find($id);
        if (!$notification) {
            return $this->json(['error' => 'Notification not found'], Response::HTTP_NOT_FOUND);
        }

        if (!$this->getUser())
            return $this->json(['message' => "vous n'êtes pas autorisé à accéder à cette ressource"], Response::HTTP_UNAUTHORIZED);

        $this->entityManager->remove($notification);
        $this->entityManager->flush();

        return $this->json(['message' => 'Notification deleted'], Response::HTTP_OK);
    }
}
