<?php
namespace App\Service;

class RestoreService
{
    public function restoreBackup($filePath)
    {
        $command = 'mysql --user=%s --password=%s --host=%s %s < %s';

        $databaseUrl = parse_url($_ENV['DATABASE_URL']);
        $command = sprintf(
            $command,
            $databaseUrl['user'],
            $databaseUrl['pass'],
            $databaseUrl['host'],
            substr($databaseUrl['path'], 1),
            $filePath
        );

        exec($command);
    }
}