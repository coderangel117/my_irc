<?php

namespace App\Enum;

class UserStatus
{
    public const Online = 'Online';
    public const DoNotDisturb ='Do not disturb';
    public const Ghost = 'Ghost';
    public const Inactive = 'Inactive';
}
