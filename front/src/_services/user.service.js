import Axios from './caller.service'


let users = () => {
    return Axios.get('/user/')
        .then((response) => {
            return response
        })
}

let userDetail = (id) => {
    return Axios.get(`/user/${id}`)
        .then((response) => {
            return response
        })
}
let userUpdate = (id, data) => {
    return Axios.put(`/user/${id}`, data)
    .then((response) => {
        return response
        })
    }

let userDelete = (id) => {
    return Axios.delete(`/user/${id}`)
    .then((response) => {
        return response
        })
    }



export const userService = {
    users,
    userDetail,
    userUpdate,
    userDelete,

}