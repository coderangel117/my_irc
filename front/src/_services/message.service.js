import Axios from './caller.service'


let GetConversations = () => {
    return Axios.get('/message/conversations')
        .then((response) => {
            return response.data
        })
}

let getConversationWithUser = (id) => {
    return Axios.get(`message/user/${id}`)
        .then((response) => {
            console.log(response.data)
            return(response.data)
        })
}

let sendMessage = (data) => {
    data = data.sentMessage
    return Axios.post('/message', data)
        .then((response) => {
            return response.data
        })
}

export const messageService = {
    GetConversations,getConversationWithUser, sendMessage
}