import {useEffect, useState} from 'react';
import {messageService, userService} from '../_services/index.js';
import ConversationPanel from './ConversationPanel';

const Navbar = () => {
    const [conversations, setConversations] = useState([]);
    const [selectedUser, setSelectedUser] = useState(null);
    const [searchTerm, setSearchTerm] = useState('');
    const [searchResults, setSearchResults] = useState([
        {
            id: 0,
            username: '',
            role: ''
        }
    ]);
    const [users, setUsers] = useState([
        {
            id: 0,
            username: '',
            role: ''
        }
    ]);
    useEffect(() => {
        const fetchData = async () => {
            try {
                const userConversations = await messageService.GetConversations();
                if (Array.isArray(userConversations)) {
                    setConversations(userConversations);
                } else {
                    console.error('Expected an array but got:', userConversations);
                }
            } catch (error) {
                console.error('Error fetching conversations:', error);
            }
        };

        fetchData();
    }, []);

    const fetchUsers = async (value) => {
        try {
            const usersList = userService.users()
                .then(
                    (response) => {
                        console.log('users:',response.data);
                        setUsers(response.data);
                        setSearchResults(users.filter(user => user.username.toLowerCase().includes(value)))
                        console.log('résultats:', searchResults);
                    },
                    (error) => {
                        console.error('Error fetching users:', error);
                    }
                )
                .then(

                );
        } catch (error) {
            console.error('Error fetching users:', error);
        }
    }
    const handleUserClick = async (user) => {
        setSelectedUser(user);
    };

    const handleSearch = (e) => {
        e.preventDefault()
        // console.log(e.target.value);
        const value = e.target.value;
        setSearchTerm(value);
        fetchUsers(value);
    };
    return (
        <div className='col-5 px-0'>
            <div className='bg-withe'>
                <div className='bg-gray px-4 py-3 bg-light'>
                    <input
                        type="text"
                        placeholder="Rechercher..."
                        className="form-control"
                        value={searchTerm}
                        onChange={handleSearch}
                    />
                </div>
                <div className="list-users">
                    {searchResults.map((user) => (
                        <p key={user.id} onClick={() => handleUserClick(user)}>
                            {user.username}
                        </p>
                    ))}
                </div>
                <h2>Liste de vos conversations</h2>
                <div className='messages-box'>
                    <div className='list-group rounded-0'>
                        {Array.isArray(conversations) && conversations.map((user) => (
                            <p key={user.id} onClick={() => handleUserClick(user)}>
                                {user.username}
                            </p>
                        ))}
                    </div>
                </div>
            </div>
            <div className='conversation'>
            {selectedUser && <ConversationPanel user={selectedUser}/>}
            </div>
        </div>
    );
};

export default Navbar;
