import { Navigate } from 'react-router-dom';
import { accountService } from '../_services';

const PublicRoute = ({ children }) => {
    if (accountService.isLoggedIn()) {
        return <Navigate to="/" replace />;
    }
    return children;
};

export default PublicRoute;
