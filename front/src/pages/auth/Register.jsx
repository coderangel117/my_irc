import React, {useState} from 'react';
import {accountService} from "../../_services/index.js";
import {useNavigate} from "react-router-dom";

import './auth.css';

const Register = () => {


    let navigate = useNavigate();
    const [credentials, setCredentials] = useState({
        username: "",
        password: ""
    });

    const onChange = (e) => {
        setCredentials({
            ...credentials,
            [e.target.name]: e.target.value
        });
    }

    const SubmitForm = (e) => {
        e.preventDefault();
        accountService.register(credentials)
            .then(data => {
                navigate("/login", {replace: true});
            })
            // .catch(error => console.error(error.response.data.message));
    }

    return (
        <div>
            <form onSubmit={SubmitForm}>
                <div className="group">
                    <label htmlFor="username">Identifiant</label>
                    <input type="text" name="username" value={credentials.username} onChange={onChange}/>
                </div>
                <div className="group">
                    <label htmlFor="password">Mot de passe</label>
                    <input type="password" name="password" value={credentials.password} onChange={onChange}/>
                </div>
                <div className="group">
                    <button>Connexion</button>
                </div>
            </form>
        </div>
    );
};

export default Register;